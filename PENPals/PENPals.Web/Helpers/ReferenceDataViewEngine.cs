﻿using PENPals.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PENPals.Web.Helpers
{
	public class ReferenceDataViewEngine : RazorViewEngine
	{
		public ReferenceDataViewEngine() : base()
		{
			ViewLocationFormats = new[] {
		"~/Views/{1}/{0}.cshtml",
		"~/Views/{1}/{0}.vbhtml",
		"~/Views/Shared/{0}.cshtml",
		"~/Views/Shared/{0}.vbhtml" };

			PartialViewLocationFormats = new[] {
		"~/Views/{1}/{0}.cshtml",
		"~/Views/{1}/{0}.vbhtml",
		"~/Views/Shared/{0}.cshtml",
		"~/Views/Shared/{0}.vbhtml"
	};

			//PartialViewLocationFormats = new[] { "{0}" };
			FileExtensions = new[] { "cshtml", "vbhtml" };
		}

		protected override IView CreateView(ControllerContext controllerContext, string viewPath, string masterPath)
		{
			var refDataController = controllerContext.Controller as Controllers.ReferenceDataControllerBase;
			// If we are dealing with a ref data controller
			if (refDataController != null)
			{
				var fileNameWithPathStripped = viewPath.Substring(viewPath.LastIndexOf(@"/") + 1);
				var refDataTable = refDataController.TypeName;
				var path = GetViewPaths(fileNameWithPathStripped, refDataTable).First(x => base.FileExists(controllerContext, x));

				return base.CreateView(controllerContext,
					path,
					masterPath);
			}
			return base.CreateView(controllerContext, viewPath, masterPath);

		}

		protected override bool FileExists(ControllerContext controllerContext, string virtualPath)
		{
			var refDataController = controllerContext.Controller as Controllers.ReferenceDataControllerBase;
			// If we are dealing with a ref data controller
			if (refDataController != null)
			{
				var fileNameWithPathStripped = virtualPath.Substring(virtualPath.LastIndexOf(@"/") + 1);
				var fileExists = GetViewPaths(fileNameWithPathStripped, refDataController.TypeName).Any(x => base.FileExists(controllerContext, x));
				return fileExists;
			}
			return base.FileExists(controllerContext, virtualPath);
		}

		private IEnumerable<string> GetViewPaths(string pageType, string referenceDataTableName)
		{
			const string prefix = "~/Views/ReferenceDataManagement/";
			//const string fileTypes = ".cshtml";

			return new List<string>
				   {
					   string.Format("{0}{1}/{2}", prefix, referenceDataTableName, pageType),
					   string.Format("{0}Default/{1}", prefix, pageType),
				   };
		}
	}
}
