﻿using BSU.Externals.IDM;
using MNIT.ApplicationAuditing;
using MNIT.ErrorLogging;
using MNIT.Web.Security;
using MNIT.Web.Security.Models;
using PENPals.Domain;
using PENPals.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PENPals.Web.Helpers
{
	public class SecurityHelper : ISecurityHelper<int>
	{
		private readonly IErrorLogger<int> _errorLogger;
		private readonly IApplicationAuditor _applicationAuditor;
		private readonly IRepository _repo;

		public SecurityHelper(IErrorLogger<int> errorLogger, IApplicationAuditor applicationAuditor, IRepository repo)
		{
			_errorLogger = errorLogger;
			_applicationAuditor = applicationAuditor;
			_repo = repo;
		}

		public void AuthenticationFailure(IAppUser<int> user, AppCredentials credential)
		{
			int? userId = null;
			if (user != null)
			{
				userId = user.ID as int?;
			}

			_applicationAuditor.AuditAction(
				new ApplicationAudit
				{
					Action = "LoginFailed",
					ActionDetail = credential.UserName,
					AuditTimeUtc = DateTime.UtcNow,
					ClientAddress = System.Web.HttpContext.Current.Request.UserHostAddress,
					SourceApplication = "PENPals",
					UserName = credential.UserName,
					EntityType = "User",
					SourceApplicationEntityId = userId,
					SourceApplicationUserId = userId
				});
		}

		public void AuthenticationSuccess(IAppUser<int> user, AuthenticationResult authResult)
		{
			_applicationAuditor.AuditAction(
				new ApplicationAudit
				{
					Action = "LoginSuccess",
					ActionDetail = user.UserName,
					AuditTimeUtc = DateTime.UtcNow,
					ClientAddress = System.Web.HttpContext.Current.Request.UserHostAddress,
					SourceApplication = "PENPals",
					UserName = user.UserName,
					EntityType = "User",
					SourceApplicationEntityId = user.ID,
					SourceApplicationUserId = user.ID
				});

			StoreIdentity(new MnitIdentity<int>(user, DateTime.Now, 0));
		}

		public IAppUser<int> CreateDefaultUserProfile(AppCredentials credential, AuthenticationResult authResult)
		{
			return null; //Or create new user in the database and return that?
		}

		public void Logout()
		{
			System.Web.HttpContext.Current.Cache.Remove("User" + (System.Web.HttpContext.Current.User.Identity as MnitIdentity<int>).UserID);
		}

		public MnitIdentity<int> RetrieveIdentity(dynamic userID)
		{
			var user = System.Web.HttpContext.Current.Cache["User" + userID] as MnitIdentity<int>;
			if (user == null)
			{
				var dbUser = _repo.GetById<int, User>(userID);
				if (!dbUser.IsActive)
				{
					return null;
				}
				user = new MnitIdentity<int>(dbUser, DateTime.Now, 0);
				StoreIdentity(user);
			}
			return user;
		}

		public List<string> RetrieveUserRoles(IAppUser<int> user)
		{
			if (user != null && user.IsAdmin)
			{
				return new List<string>() { "Admin" };
			}

			return null;
		}

		public void StoreIdentity(MnitIdentity<int> authUser)
		{
			System.Web.HttpContext.Current.Cache["User" + authUser.UserID] = authUser;
		}
	}
}
