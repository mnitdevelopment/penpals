﻿
function GenerateViewsAndController{
	param([string[]]$insertOnlytablesToGenerate, [string[]]$viewsToGenerate, [bool]$generateController, [string]$insertOnly, [string]$currentDir)

	foreach($table in $insertOnlytablesToGenerate){

		$folderExists = $false
		foreach($folder in $dte.ActiveDocument.ProjectItem.ContainingProject.ProjectItems){
			if($folder.Name -eq "Views"){
				foreach($subFolder in $folder.ProjectItems){
					if($subFolder.Name -eq "$table"){
						$folderExists = $true
					}
				}
				if($folderExists -eq $false){
					$folder.ProjectItems.AddFolder("$table")
				}
			}
		}

		foreach($view in $viewsToGenerate){

			$path = "Views\"
			$param = "!!TableNameParam!$table"
			$outputFile = "$currentDir\$path$table\$view.cshtml"
			$template = "$currentDir\T4Templates\$view.tt"

			#run the template and put the output into the correct location
			& "C:\Program Files (x86)\Common Files\microsoft shared\TextTemplating\14.0\TextTransform.exe" -out $outputFile -a $param $template

			#modify project file to add views to project if not existing
			try{
				$dte.ActiveDocument.ProjectItem.ContainingProject.ProjectItems.AddFromFile($outputFile)
			}
			catch
			{
				#swallow
			}
		}

		if($generateController -eq $True){
		
			
			$path = "Controllers\"
			$param = "!!TableNameParam!$table,$insertOnly"
			$fileSuffix = "Controller.cs"
			$outputFile = "$currentDir\$path$table$fileSuffix"
			$template = "$currentDir\T4Templates\Controller.tt"

			#run the template and put the output into the correct location
			& "C:\Program Files (x86)\Common Files\microsoft shared\TextTemplating\14.0\TextTransform.exe" -out $outputFile -a $param $template

			#modify project file to add views to project if not existing
			try{
				$dte.ActiveDocument.ProjectItem.ContainingProject.ProjectItems.AddFromFile($outputFile)
			}
			catch
			{
				#swallow
			}
		}
	}
}

$insertOnlytablesToGenerate = @("CoreRecord")
$regulartablesToGenerate = @("User")
$viewsToGenerate = @("Edit") #options are Index and/or Edit
$generateController = $True
$currentDir = Split-Path $MyInvocation.MyCommand.Path

GenerateViewsAndController -insertOnlytablesToGenerate $insertOnlytablesToGenerate -viewsToGenerate $viewsToGenerate -generateController $generateController -insertOnly "true" -currentDir $currentDir
GenerateViewsAndController -insertOnlytablesToGenerate $regulartablesToGenerate -viewsToGenerate $viewsToGenerate -generateController $generateController -insertOnly "false" -currentDir $currentDir
