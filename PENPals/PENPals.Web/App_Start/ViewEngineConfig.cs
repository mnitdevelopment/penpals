﻿using PENPals.Web.Helpers;
using System.Linq;
using System.Web.Mvc;
using System.Web.WebPages;

namespace PENPals.Web.App_Start
{
	public class ViewEngineConfig
	{
		internal static void RegisterViewEngines()
		{
			DisplayModeProvider.Instance.Modes.Remove(DisplayModeProvider.Instance.Modes.Single(m => m.DisplayModeId == "Mobile"));
			ViewEngines.Engines.Remove(ViewEngines.Engines.FirstOrDefault(e => e.GetType().Name == "WebFormViewEngine"));
			ViewEngines.Engines.Add(new ReferenceDataViewEngine());
		}
	}
}
