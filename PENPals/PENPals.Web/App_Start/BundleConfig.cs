﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace PENPals.Web.App_Start
{
	public class BundleConfig
	{
		// For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new StyleBundle("~/Content/css")
				.Include("~/Content/bootstrap.css")
				.Include("~/Content/font-awesome.css")
				.Include("~/Content/MNHHS.css")
				.Include("~/Content/jquery.datetimepicker.css")
				);

			bundles.Add(new ScriptBundle("~/Scripts/core")
				.Include("~/Scripts/jquery-{version}.js")
				.Include("~/Scripts/bootstrap.js")
				.Include("~/Scripts/moment.js")
				.Include("~/Scripts/knockout-{version}.js")
				.Include("~/Scripts/jquery.datetimepicker.js")
				.Include("~/Scripts/MNHHS-core.js")
				.Include("~/Scripts/MNHHS-PatientSearch.js"));
		}
	}
}
