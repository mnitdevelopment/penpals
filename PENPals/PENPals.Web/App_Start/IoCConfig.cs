﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using MassTransit;
using MassTransit.BusConfigurators;
using MNIT.ApplicationAuditing;
using MNIT.ApplicationAuditing.MassTransit;
using MNIT.ErrorLogging;
using MNIT.ErrorLogging.FilterAttributes;
using MNIT.ErrorLogging.MassTransit;
using MNIT.Externals.ClientDirectory;
using MNIT.Web.EventAggregator;
using MNIT.Web.Security;
using PENPals.Web.Attributes;
using PENPals.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;

namespace PENPals.Web.App_Start
{
	public class IoCConfig
	{
		public static IContainer RegisterIoC()
		{
			var builder = new ContainerBuilder();

			builder.RegisterControllers(Assembly.GetExecutingAssembly());

			RegisterEventHandlers(builder);

			RegisterBus(builder);
			RegisterApplicationAuditing(builder);
			RegisterErrorLogging(builder);

			RegisterAuthentication(builder);

			RegisterFilters(builder);

			builder.Register(b => new ClientDirectorySearcher(new ConfigFileConfiguration())).As<IClientDirectorySearch>();

			Domain.IoCConfig.BuildDomainContainer(builder);

			var container = builder.Build();

			var resolver = new AutofacDependencyResolver(container);
			DependencyResolver.SetResolver(resolver);

			return container;
		}

		private static void RegisterFilters(ContainerBuilder builder)
		{
			builder.Register(c => new MvcErrorLoggingFilterAttribute(c.Resolve<IErrorLogger<int>>()))
		.AsExceptionFilterFor<Controller>().InstancePerRequest();
			builder.RegisterFilterProvider();

			//Do not register WebAPI filters as we aren't using WebAPI (yet)
			//builder.Register(c => new WebApiExceptionFilterAttribute(c.Resolve<IErrorLogger>()))
			//    .AsWebApiExceptionFilterFor<ApiController>().InstancePerRequest();
			//builder.RegisterWebApiFilterProvider(config);
		}

		private static void RegisterAuthentication(ContainerBuilder builder)
		{
			builder.RegisterType<SecurityHelper>().As<ISecurityHelper<int>>();
			builder.Register(context => new LoginManager<int>(context.Resolve<ISecurityHelper<int>>())).As<ILoginManager<int>>();
		}

		private static void RegisterErrorLogging(ContainerBuilder builder)
		{
			var applicationName = "PENPals";
			// Setup our logging component. We take an IEnumerable here so multiple Error Repositories can be 
			// registered. i.e. One for the MT component + a local db write
			builder.Register(context =>

				new ErrorLogger<int>(context.Resolve<IEnumerable<IErrorRepository>>(), applicationName)

			).As<IErrorLogger<int>>();

			// Register our MT and local error repositories
			builder.RegisterType<MassTransitErrorRepository>().As<IErrorRepository>();
		}

		private static void RegisterApplicationAuditing(ContainerBuilder builder)
		{
			var applicationName = "PENPals";
			// Setup our logging component. We take an IEnumerable here so multiple audit Repositories can be 
			// registered. i.e. One for the MT component + a local db write
			builder.Register(context =>

				new ApplicationAuditor(context.Resolve<IEnumerable<IAuditRepository>>(), applicationName)

			).As<IApplicationAuditor>();

			// Register our MT
			builder.RegisterType<MassTransitAuditRepository>().As<IAuditRepository>();
		}

		private static void RegisterBus(ContainerBuilder builder)
		{
			// Register a bus for our MassTransit Repositories to use
			builder.Register<IBus>(context =>
			{

				IBusControl busControl = Bus.Factory.CreateUsingRabbitMq(sbc =>
				{
					var endpointConfig = MNIT.MassTransit.ServiceBusConfig.LoadFromConfig("ServiceBus");
					var host = sbc.Host(new Uri(endpointConfig.EndpointAddress), h =>
					{
						h.Username(endpointConfig.RabbitMqUserName);
						h.Password(endpointConfig.RabbitMqPassword);
					});

					sbc.UseRetry(Retry.Immediate(5));

				});

				//busControl.Start();
				return busControl;
			}).SingleInstance()
			//.As<IBus>()
			//.AsImplementedInterfaces()
			;


		}

		private static void RegisterEventHandlers(ContainerBuilder builder)
		{
			builder.RegisterType<EventAggregator>().As<IEventAggregator>().SingleInstance();
			builder.RegisterAssemblyTypes(AppDomain.CurrentDomain.GetAssemblies())
				.As<IHandle>().SingleInstance().AutoActivate();
		}
	}
}
