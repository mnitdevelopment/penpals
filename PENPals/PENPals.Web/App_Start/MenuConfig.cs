﻿using PENPals.Web.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Routing;

namespace PENPals.Web.App_Start
{
	public class SimpleMenuItem
	{
		public string LinkText { get; set; }
		public string LinkUrl { get; set; }
		public bool AdminOnly { get; set; }
	}

	public class MenuConfig
	{

		internal static void RegisterMenu(HttpContext ctx)
		{
			if (ctx.Cache["MenuItems"] != null)
			{
				return;
			}
			var urlHelper = new UrlHelper(ctx.Request.RequestContext);
			var menuItems = new List<SimpleMenuItem>();

			var controllers = Assembly.GetExecutingAssembly().ExportedTypes.Where(t => t.IsSubclassOf(typeof(Controller)));
			foreach (var controller in controllers)
			{
				var menuMethods = controller.GetRuntimeMethods().Where(m => m.IsPublic &&
																			(m.ReturnType == typeof(ActionResult) || m.ReturnType == typeof(Task<ActionResult>)) &&
																			m.CustomAttributes.Any(a => a.AttributeType == typeof(MenuItemAttribute)));

				foreach (var method in menuMethods)
				{
					var menuAttribute = method.GetCustomAttributes(typeof(MenuItemAttribute), true).FirstOrDefault();
					var attr = (menuAttribute as MenuItemAttribute);
					if (attr != null)
					{
						var prettyName = attr.DisplayName;
						menuItems.Add(new SimpleMenuItem
						{
							LinkText = prettyName,
							LinkUrl = urlHelper.RouteUrl("Default", new
							{
								action = method.Name,
								controller = controller.Name.Replace("Controller", string.Empty)
							}),
							//LinkUrl = urlHelper.Action(method.Name, 
							//                           controller.Name.Replace("Controller", string.Empty),
							//                           new { name = "Default" }),
							AdminOnly = attr.IsAdministrationMenu
						});
					}
				}

			}
			ctx.Cache.Add("MenuItems", menuItems, null, DateTime.MaxValue, Cache.NoSlidingExpiration, CacheItemPriority.High, null);
		}
	}
}
