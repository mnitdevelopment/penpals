﻿using Autofac;
using MNIT.ErrorLogging;
using MNIT.Web.Security;
using PENPals.Web.App_Start;
using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PENPals.Web
{
	public class MvcApplication : HttpApplication
	{
		protected void Application_Start()
		{
			container = IoCConfig.RegisterIoC();
			AreaRegistration.RegisterAllAreas();
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			ViewEngineConfig.RegisterViewEngines();
			GlobalFilters.Filters.Add(new AuthorizeAttribute());
		}

		private static IContainer container;
		internal static IContainer Container
		{
			get { return container; }
		}

		protected void Application_AuthenticateRequest(object sender, EventArgs e)
		{
			var errorHandler = DependencyResolver.Current.GetService<IErrorLogger<int>>();
			try
			{
				var loginManager = DependencyResolver.Current.GetService<ILoginManager<int>>();
				if (!(loginManager.AuthenticateRequest(Context)))
				{
					return;
				}
			}
			catch (Exception exception)
			{
				errorHandler.LogError(new Error
				{
					FullErrorString = exception.ToString()
				});
			}
		}

		protected void Application_BeginRequest()
		{
			//override date formatting to the one we want!
			var newCulture = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
			newCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
			newCulture.DateTimeFormat.LongDatePattern = "dd-MMM-yyyy";
			newCulture.DateTimeFormat.DateSeparator = "/";

			newCulture.DateTimeFormat.ShortTimePattern = "hh:mm tt";
			newCulture.DateTimeFormat.LongTimePattern = "hh:mm tt";
			newCulture.DateTimeFormat.TimeSeparator = ":";

			Thread.CurrentThread.CurrentCulture = newCulture;

			//First app request pays the price of registering the menu items.
			MenuConfig.RegisterMenu(HttpContext.Current);
		}
	}
}
