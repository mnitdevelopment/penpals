﻿using System;
using System.Web.Mvc;

namespace PENPals.Web.Attributes
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class MenuItemAttribute : ActionFilterAttribute
	{
		public bool IsAdministrationMenu { get; set; }
		public string DisplayName { get; set; }

	}
}
