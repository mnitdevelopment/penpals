﻿using MNIT.ApplicationAuditing;
using MNIT.ErrorLogging;
using MNIT.Web.Security;
using MNIT.Web.Security.Models;
using PENPals.Domain;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PENPals.Web.Controllers
{
	[Authorize]
	public class AccountController : Controller
	{
		private readonly IErrorLogger<int> _errorLogger;
		private readonly IApplicationAuditor _applicationAuditor;
		private readonly ILoginManager<int> _login;
		private readonly ISecurityHelper<int> _security;
		private readonly IRepository _repo;

		public AccountController(IErrorLogger<int> errorLogger, IApplicationAuditor applicationAuditor, ILoginManager<int> loginManager, ISecurityHelper<int> secHelper, IRepository repo)
		{
			_errorLogger = errorLogger;
			_applicationAuditor = applicationAuditor;
			_login = loginManager;
			_security = secHelper;
			_repo = repo;
		}

		[AllowAnonymous]
		public ActionResult Login(string returnUrl)
		{
			ViewBag.ReturnUrl = returnUrl;
			return View();
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Login(LoginModel model, string returnUrl)
		{
			if (!ModelState.IsValid)
			{
				// We aren't valid, lets go back
				return View(model);
			}

			// Attempt to get user's logininfo and base record
			LoginInfo loginInfo = null; //TODO: DB Call to get
			Domain.Models.User user = await _repo.GetUserByUsername(model.UserName);

			var credential = new AppCredentials(model.UserName, model.Password);
			if (_login.Login(credential, user, loginInfo))
			{

				if (Url.IsLocalUrl(returnUrl) && !string.IsNullOrEmpty(returnUrl) && returnUrl != "/")
				{
					return Redirect(returnUrl);
				}

				//else go to the homepage
				return Redirect("/");

			}
			else
			{
				ModelState.AddModelError("", "The user name or password provided is incorrect.");
			}


			return View(model);
		}


		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public ActionResult LogOff()
		{
			var userID = new Nullable<int>();
			var username = "unknown";

			var genericIdentity = (HttpContext.User.Identity as System.Web.Security.FormsIdentity);

			if (genericIdentity != null && genericIdentity.Ticket != null && !string.IsNullOrEmpty(genericIdentity.Ticket.UserData))
			{
				var identity = System.Web.Helpers.Json.Decode<MnitIdentity<int>>(genericIdentity.Ticket.UserData);
				userID = (identity.UserID as int?);
				username = identity.Username;
			}

			_applicationAuditor.AuditAction(
				new ApplicationAudit
				{
					Action = "Logoff",
					ActionDetail = username,
					AuditTimeUtc = DateTime.UtcNow,
					ClientAddress = Request.UserHostAddress,
					SourceApplication = "PENPals",
					UserName = username,
					EntityType = "User",
					SourceApplicationEntityId = userID,
					SourceApplicationUserId = userID
				});

			_login.Logout(() => _security.Logout());
			return RedirectToAction("Login");

		}
	}
}
