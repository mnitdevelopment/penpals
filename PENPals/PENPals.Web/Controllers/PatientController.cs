﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using PENPals.Web.Attributes;
using PENPals.Domain;
using PENPals.Domain.Models;
using PENPals.Domain.Interfaces;
using MNIT.Web.Security.Models;
using MNIT.Externals.ClientDirectory;
using System.Net;
using System.Collections.Generic;
using System;
using PENPals.Domain.Helpers;

namespace PENPals.Web.Controllers
{
	/// <summary>
	/// A controller which represents the Patient table.
	/// </summary>
	[Authorize]
	public partial class PatientController : Controller
	{
		private readonly IReferenceData _refData;
		private readonly IRepository _repo;
		private readonly IClientDirectorySearch _cdSearcher;
		private readonly IPatientMapper _patientMapper;

		public PatientController(IReferenceData refData, IRepository repo, IClientDirectorySearch cdSearcher, IPatientMapper patientMapper)
		{
			_refData = refData;
			_repo = repo;
			_cdSearcher = cdSearcher;
			_patientMapper = patientMapper;
		}


		[HttpGet]
		[MenuItem(DisplayName = "Patients")]
		public async Task<ActionResult> Index()
		{
			var records = await _repo.Get<Patient>();
			var refGenders = await _refData.Get<Gender>();
			var refIndigenousStatus = await _refData.Get<IndigenousStatu>();
			ViewBag.refFacilities = new SelectList((await _refData.GetFacilities()).Where(f => f.IsActive).OrderBy(f => f.Name), "HICCode", "Name");

			foreach (var record in records)
			{
				record.Gender = refGenders.FirstOrDefault(s => s.ID == record.GenderId);
				if (record.IndigenousStatusId.HasValue)
				{
					record.IndigenousStatu = refIndigenousStatus.FirstOrDefault(s => s.ID == record.IndigenousStatusId);

				}
			}

			return View(records);
		}


		[HttpGet]
		public async Task<ActionResult> Edit(int id)
		{
			var record = await _repo.GetByIdAsync<int, Patient>(id);
			await SetupReferenceDataForEdit(record.GenderId, record.IndigenousStatusId);
			ViewBag.CurrentRecordHashCode = record.GetHashCode();
			return View(record);
		}

		[HttpPost]
		public async Task<ActionResult> Edit(Patient data, int? CurrentRecordHashCode)
		{
			var model = await _repo.GetByIdAsync<int, Patient>(data.ID);
			if (CurrentRecordHashCode.HasValue && model != null && CurrentRecordHashCode != model.GetHashCode())
			{
				return new HttpStatusCodeResult(System.Net.HttpStatusCode.Conflict, string.Format("Record {0} has been changed by another user.", data.ID));
			}

			if (model == null)
			{
				model = new Patient
				{
					PatientId = System.Guid.NewGuid()
				};
			}

			//Remove the insert only fields if we're dealing with a new record
			if (data.ID == 0)
			{
				ModelState.Remove("ID");
				ModelState.Remove("PatientId");
				ModelState.Remove("ChangedAt");
				ModelState.Remove("ChangedById");
				ModelState.Remove("BaseVersion");
				ModelState.Remove("CurrentRecordHashCode");
			}
			var tryUpdateModelExclusions = new string[] { "ID", "PatientId", "ChangedAt", "ChangedById", "BaseVersion" };
			if (data.ID != 0) //existing record
			{
				tryUpdateModelExclusions = null;
			}
			if (TryUpdateModel(model, "", null, tryUpdateModelExclusions))
			{
				await _repo.InsertForInsertOnly(model, (User.Identity as MnitIdentity<int>).ID);
				return RedirectToAction("Index");
			}

			//model bind error
			await SetupReferenceDataForEdit(model.GenderId, model.IndigenousStatusId);
			ViewBag.CurrentRecordHashCode = CurrentRecordHashCode;
			return View(model);
		}

		[HttpGet]
		public async Task<ActionResult> New()
		{
			await SetupReferenceDataForEdit(new int?(), new int?());
			return View("Edit", new Patient());
		}

		[HttpPost]
		public async Task<ActionResult> Delete(int id, int? CurrentRecordHashCode)
		{
			//todo make sure we get the latest record for this id
			var model = await _repo.GetByIdAsync<int, Patient>(id);
			if (!CurrentRecordHashCode.HasValue && model != null && CurrentRecordHashCode != model.GetHashCode())
			{
				return new HttpStatusCodeResult(System.Net.HttpStatusCode.Conflict, string.Format("Record {0} has been changed by another user.", id));
			}


			await _repo.DeleteForInsertOnly<int, Patient>(id, (User.Identity as MnitIdentity<int>).ID);
			return RedirectToAction("Index");
		}


		private async Task SetupReferenceDataForEdit(int? selectedGenderId, int? selectedIndigenousStatuId)
		{
			ViewBag.refGenders = new SelectList((await _refData.Get<Gender>()).Where(r => r.IsActive), "ID", "Name", selectedGenderId);
			ViewBag.refIndigenousStatus = new SelectList((await _refData.Get<IndigenousStatu>()).Where(r => r.IsActive), "ID", "Name", selectedIndigenousStatuId);

		}

		[HttpGet]
		public async Task<ActionResult> SearchByUr(string facilityHicCode, string urn)
		{
			var facilities = await _refData.GetFacilities();
			if (!facilities.Any(f => f.HICCode == facilityHicCode))
			{
				return Json(null, JsonRequestBehavior.AllowGet);
			}
			var cdData = await _cdSearcher.SearchAsync(facilityHicCode, urn);
			if (cdData != null)
			{
				var urns = "";
				foreach (var ur in cdData.URNumbers)
				{
					var facility = facilities.FirstOrDefault(f => f.HICCode == ur.FacilityHicCode);
					if (facility != null)
					{
						urns += facility.URPrefix + ur.URNumber + ", ";
					}
				}
				if (string.IsNullOrEmpty(urns)) //no metro north UR
				{
					return new HttpStatusCodeResult(HttpStatusCode.NoContent);
				}
				int sexCode;
				Sex sexValue = Sex.Unknown;
				if (int.TryParse(cdData.Sex, out sexCode))
				{
					sexValue = (Sex)sexCode;
				}
				var cdResult = new
				{
					Name = string.Format("{0}, {1}", cdData.Surname ?? "", cdData.FirstName ?? ""),
					URNs = urns.TrimEnd(',', ' '),
					DateOfBirth = cdData.DOB.HasValue ? cdData.DOB.Value.ToShortDateString() : "DoB unknown",
					Sex = sexValue.ToString(),
					CDN = cdData.CDN
				};
				return Json(cdResult, JsonRequestBehavior.AllowGet);
			}

			return new HttpStatusCodeResult(HttpStatusCode.NoContent);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> ConfirmPatient(string cdn)
		{
			var currentUser = (User.Identity as MnitIdentity<int>).ID;
			var cdPerson = await _cdSearcher.GetPersonByClientDirectoryIDAsync(cdn);
			var patient = await _patientMapper.MapPatient(cdPerson);

			var urCollection = await _patientMapper.MapUrs(cdPerson);

			var existingCoreRecordId = await _repo.GetAndUpdateExistingPatientAndCoreRecord(patient, urCollection, currentUser);

			if (existingCoreRecordId.HasValue)
			{
				return RedirectToAction("Edit", "CoreRecord", new { id = existingCoreRecordId.Value });
			}
			else
			{
				var CoreRecord = new CoreRecord
				{
					CoreRecordId = Guid.NewGuid(),
					IsActive = true,
					ChangedById = currentUser
				};
				var PENPalsId = await _repo.InsertNewPatientAndCoreRecord(patient, urCollection, CoreRecord, currentUser);
				return RedirectToAction("Edit", "CoreRecord", new { id = PENPalsId });
			}
		}
	}
}
