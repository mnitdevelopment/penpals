﻿using MNIT.ApplicationAuditing;
using MNIT.ErrorLogging;
using PENPals.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PENPals.Web.Controllers
{
	[Authorize]
	public class HomeController : Controller
	{

		private readonly IErrorLogger<int> _errorLogger;
		private readonly IApplicationAuditor _applicationAuditor;
		private readonly IRepository _repo;
		public HomeController(IErrorLogger<int> errorLogger, IApplicationAuditor applicationAuditor, IRepository repo)
		{
			_errorLogger = errorLogger;
			_applicationAuditor = applicationAuditor;
			_repo = repo;
		}

		// GET: Home
		public ActionResult Index()
		{
			//_errorLogger.LogError(new Error
			//{
			//	AdditionalInformation = "info",
			//	ClientAddress = "1",
			//	ErrorHash = "hash",
			//	ErrorTime = DateTime.Now,
			//	FullErrorString = "error",
			//	SourceApplication = "PENPals",
			//	SourceApplicationUserId = 2,
			//	UserName = "user",
			//	RootErrorMessage = "error",
			//	ID = 1
			//});
			//_applicationAuditor.AuditAction(new ApplicationAudit
			//{
			//	Action = "act",
			//	UserName = "on",
			//	SourceApplicationUserId = 1,
			//	ActionDetail = "detail",
			//	AuditTimeUtc = DateTime.UtcNow,
			//	ClientAddress = "1",
			//	EntityType = "ent",
			//	Id = 1,
			//	SourceApplication = "PENPals",
			//	SourceApplicationEntityId = 2
			//});
			return View();
		}
	}
}
