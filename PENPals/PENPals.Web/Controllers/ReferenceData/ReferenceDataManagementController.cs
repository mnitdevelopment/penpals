﻿using PENPals.Web.Attributes;
using PENPals.Domain.Interfaces;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace PENPals.Web.Controllers.ReferenceData
{
	public class ReferenceDataManagementController : Controller
	{
		[MenuItem(IsAdministrationMenu = true, DisplayName = "Reference Data")]
		[Authorize(Roles = "Admin")]
		public ActionResult Index()
		{
			var refData = Assembly.GetAssembly(typeof(IReferenceDataTable)).DefinedTypes.Where(d => d.ImplementedInterfaces.Contains(typeof(IReferenceDataTable)));
			return View(refData.Select(d => d.Name));
		}
	}
}
