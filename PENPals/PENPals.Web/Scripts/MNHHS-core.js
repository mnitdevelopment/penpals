﻿var PENPals = window.PENPals || {};
window.PENPals = PENPals;

// Track basic JavaScript errors
window.addEventListener('error', function (e) {
    if ("ga" in window) {
        ga('send', 'exception', {
            'exDescription': 'Javascript Error: ' + e.message + ':  ' + e.filename + ':  ' + e.lineno,
            'exFatal': false
        });
    }
});

//Send all ajax events to ga
$(document).ajaxStart(function (data) {
    if ("ga" in window) {
        ga('send', 'event', 'ajax', 'Start');
    }
}).ajaxError(function (e, request, settings) {
    if ("ga" in window) {
        ga('send', 'exception', {
            'exDescription': 'AJAX Error:  ' + settings.url + ':  ' + e.result,
            'exFatal': false
        });
    }
});

//Set global date functions to use moment
Date.parseDate = function (input, format) {
    return moment(input, format).toDate();
};
Date.prototype.dateFormat = function (format) {
    return moment(this).format(format);
};
