﻿PENPals.PatientSearch = {};

PENPals.PatientSearch.VM = function (configuration) {

    var self = this;
    self.urn = ko.observable('');
    self.searchResultCDN = ko.observable(null);
    self.searchResultName = ko.observable(null);
    self.searchResultUrns = ko.observable(null);
    self.searchResultDoB = ko.observable(null);
    self.searchResultSex = ko.observable(null);
    self.hasResults = ko.observable(false);
    //FYI This is not the preferred way for dealing with binding of large fieldsets - use ko mapping if are doing more than half a dozen or so.

    self.searchByUr = function () {
        self.hasResults(false);
        $.ajax({
            url: configuration.SearchByUrEndpoint,
            data: { facilityHicCode: $("#" + configuration.FacilityHicCodeSelector).val(), urn: self.urn() },
            type: 'GET',
            success: function (data) {
                if (data) {
                    self.searchResultCDN(data.CDN);
                    self.searchResultName(data.Name);
                    self.searchResultUrns(data.URNs);
                    self.searchResultDoB(data.DateOfBirth);
                    self.searchResultSex(data.Sex);
                    self.hasResults(true);
                } else {
                    self.searchResultCDN("");
                    self.searchResultName("No Metro North HHS patient record found.");
                    self.searchResultUrns("");
                    self.searchResultDoB("");
                    self.searchResultSex("");
                }
            }
        });
    }
    return self;
};
