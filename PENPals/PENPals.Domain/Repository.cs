﻿using System;
using PENPals.Domain.Interfaces;
using PENPals.Domain.Models;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using MNIT.Extensions;
using System.Threading.Tasks;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using MNIT.ApplicationAuditing;

namespace PENPals.Domain
{
	public class Repository : IRepository
	{
		private readonly IApplicationAuditor _audit;
		public Repository(IApplicationAuditor audit)
		{
			_audit = audit;
		}

		public async Task<T> GetByIdAsync<TId, T>(TId id)
		{
			using (var ctx = GetConnection())
			{
				var entity = await ctx.GetAsync<T>(id);
				return entity;
			}
		}

		public T GetById<TId, T>(TId id)
		{
			using (var ctx = GetConnection())
			{
				var entity = ctx.Get<T>(id);
				return entity;
			}
		}

		public IDbConnection GetConnection()
		{
			return new SqlConnection(ConfigurationManager.ConnectionStrings["PENPals"].ConnectionString);
		}

		public async Task<User> GetUserByUsername(string username)
		{
			using (var ctx = GetConnection())
			{
				var users = await ctx.QueryAsync<User>("select * from [User] where UserName = @Username and IsActive = 1", new { Username = username });
				return users.FirstOrDefault();
			}
		}

		public async Task<IEnumerable<T>> Get<T>(bool activeOnly = true)
		{
			using (var ctx = GetConnection())
			{
				if (typeof(T).GetProperties().Any(p => p.Name == "BaseVersion"))
				{
					return await ctx.QueryAsync<T>(string.Format("select * from vwCurrent{0}{1}", typeof(T).Name, activeOnly ? " where IsActive = 1" : ""));
				}
				return await ctx.GetListAsync<T>(activeOnly ? " where IsActive = 1" : "");
			}
		}

		public async Task<bool> Update<T>(T model)
		{
			using (var conn = GetConnection())
			{
				await conn.UpdateAsync(model);
			}
			return true;
		}

		public async Task<int> Insert<T>(T model, IDbConnection existingConnection = null, IDbTransaction existingTransaction = null)
		{
			if (existingConnection != null)
			{
				return await InsertInternal(model, existingConnection, existingTransaction);
			}
			using (var conn = GetConnection())
			{
				return await InsertInternal(model, conn, existingTransaction);
			}
		}

		private async Task<int> InsertInternal<T>(T model, IDbConnection existingConnection, IDbTransaction existingTransaction)
		{
			return await existingConnection.InsertAsync<int>(model, existingTransaction);
		}

		public async Task<int> InsertForInsertOnly<T>(T model, int changedByUserId, IDbConnection existingConnection = null, IDbTransaction existingTransaction = null) where T : IInsertOnlyTable<int, int?>
		{
			if (existingConnection != null)
			{
				return await InsertForInsertOnlyInternal(model, changedByUserId, existingConnection, existingTransaction);
			}
			using (var conn = GetConnection())
			{
				return await InsertForInsertOnlyInternal(model, changedByUserId, conn, existingTransaction);
			}
		}

		private async Task<int> InsertForInsertOnlyInternal<T>(T model, int changedByUserId, IDbConnection existingConnection, IDbTransaction existingTransaction) where T : IInsertOnlyTable<int, int?>
		{
			if (existingConnection == null)
			{
				throw new ApplicationException("existingConnection is null or in an unusable state.");
			}

			if (model.ID != 0)//exiting record being 'updated'
			{
				model.BaseVersion = model.ID;
			}

			model.ChangedAt = System.DateTime.Now;
			model.ChangedById = changedByUserId;
			model.ID = 0; //ensure dapper treats it correctly as a new row.

			model.ID = await existingConnection.InsertAsync<int>(model, existingTransaction);
			return model.ID;
		}

		public async Task<bool> Delete<TId, T>(TId id, bool hardDelete)
		{
			using (var conn = GetConnection())
			{
				if (hardDelete)
				{
					_audit.AuditAction(new ApplicationAudit { Action = "Delete", AuditTimeUtc = DateTime.UtcNow, EntityType = typeof(T).Name, SourceApplication = "PENPals" });
					await conn.DeleteAsync(await GetByIdAsync<TId, T>(id));
				}
				else
				{
					await conn.ExecuteAsync(string.Format("update [{0}] set IsActive = 0 where ID = @recordId", typeof(T).Name), new { recordId = id });
				}
			}
			return true;
		}

		public async Task<bool> DeleteForInsertOnly<TId, T>(TId id, int deletedByUserId) where T : IInsertOnlyTable<int, int?>
		{
			using (var conn = GetConnection())
			{
				var record = await GetByIdAsync<TId, T>(id);
				record.IsActive = false;

				await InsertForInsertOnly(record, deletedByUserId);
			}
			return true;
		}

		public async Task<int> InsertNewPatientAndCoreRecord(Patient patient, IEnumerable<PatientUr> urCollection, CoreRecord CoreRecord, int changedByUserId)
		{
			using (var conn = GetConnection())
			{
				conn.Open();//Explictly open the connection so a transaction can be started
				using (var tran = conn.BeginTransaction())
				{
					try
					{
						var patientId = await InsertForInsertOnly(patient, changedByUserId, conn, tran);
						foreach (var ur in urCollection)
						{
							ur.PatientId = patientId;
							await Insert(ur, conn, tran);
						}
						CoreRecord.PatientId = patientId;
						var CoreRecordId = await InsertForInsertOnly(CoreRecord, changedByUserId, conn, tran);
						tran.Commit();
						return CoreRecordId;
					}
					catch
					{
						tran.Rollback();
						throw;
					}
				}
			}
		}

		public async Task<int?> GetAndUpdateExistingPatientAndCoreRecord(Patient patient, IEnumerable<PatientUr> urCollection, int changedByUserId)
		{
			using (var conn = GetConnection())
			{
				conn.Open();//Explictly open the connection so a transaction can be started
				using (var tran = conn.BeginTransaction())
				{
					try
					{
						Patient existingPatient = null;
						foreach (var ur in urCollection)
						{
							existingPatient = (await conn.QueryAsync<Patient>(sql: "GetExistingPatient",
																			param: new { facilityId = ur.FacilityId, ur = ur.URN },
																			commandType: CommandType.StoredProcedure,
																			transaction: tran)).FirstOrDefault();
							if (existingPatient != null)
							{
								break; //found a match, that'll do!
							}
						}
						if (existingPatient == null) //no matches against the local db, new guy!
						{
							return null;
						}

						var oldPatientId = existingPatient.ID;
						patient.ID = oldPatientId; //set the existing DB ID on the mapped object
						patient.PatientId = existingPatient.PatientId; //Need to make sure the unique key across the record is retained
						var newPatientId = await InsertForInsertOnly(patient, changedByUserId, conn, tran);
						foreach (var ur in urCollection)
						{
							ur.PatientId = newPatientId;
							await Insert(ur, conn, tran);
						}

						var existingCoreRecord = (await conn.QueryAsync<CoreRecord>(sql: "select * from vwCurrentCoreRecord where PatientID = @patientId",
																						 param: new { patientId = oldPatientId },
																						 transaction: tran)).First(); //If more than one is returned then that's bad

						existingCoreRecord.PatientId = newPatientId;
						existingCoreRecord.ChangedById = changedByUserId;
						var CoreRecordId = await InsertForInsertOnly(existingCoreRecord, changedByUserId, conn, tran);
						tran.Commit();
						return CoreRecordId;
					}
					catch
					{
						tran.Rollback();
						throw;
					}
				}
			}
		}
	}
}
