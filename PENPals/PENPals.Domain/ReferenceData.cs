﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PENPals.Domain.Interfaces;
using MNIT.Caching;
using Dapper;
using System.Data.SqlClient;
using PENPals.Domain.Models;

namespace PENPals.Domain
{
	public class ReferenceData : IReferenceData
	{
		private readonly ICacheService _cache;
		private readonly IRepository _repo;

		public ReferenceData(ICacheService cache, IRepository repo)
		{
			_cache = cache;
			_repo = repo;
		}
		public async Task<IEnumerable<T>> Get<T>() where T : IReferenceDataTable
		{
			var key = typeof(T).Name;
			var item = await _cache.Get<IEnumerable<T>>(key);

			if (item == null)
			{
				return await _cache.Add<IEnumerable<T>>(key, new TimeSpan(4, 0, 0), reloadCallback: () => Fetch<T>());
			}

			return item.Value;
		}

		public async Task Invalidate<T>() where T : IReferenceDataTable
		{
			await _cache.Remove(typeof(T).Name);
		}

		public async Task Update<T>(T model) where T : IReferenceDataTable
		{
			using (var conn = _repo.GetConnection())
			{
				await conn.UpdateAsync(model);
				await Invalidate<T>();
			}
		}

		public async Task Insert<T>(T model) where T : IReferenceDataTable
		{
			using (var conn = _repo.GetConnection())
			{
				await conn.InsertAsync<int>(model);
				await Invalidate<T>();
			}
		}

		public async Task<IEnumerable<Facility>> GetFacilities()
		{
			var key = "Facility";
			var item = await _cache.Get<IEnumerable<Facility>>(key);

			if (item == null)
			{
				return await _cache.Add<IEnumerable<Facility>>(key, new TimeSpan(4, 0, 0), reloadCallback: () => Fetch<Facility>());
			}

			return item.Value;
		}

		private async Task<IEnumerable<T>> Fetch<T>()
		{
			using (var conn = _repo.GetConnection())
			{
				var refList = await conn.GetListAsync<T>();
				return refList.ToList();
			}
		}

	}
}
