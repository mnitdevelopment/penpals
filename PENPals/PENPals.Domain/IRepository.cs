﻿using PENPals.Domain.Interfaces;
using PENPals.Domain.Models;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace PENPals.Domain
{
	public interface IRepository
	{
		Task<User> GetUserByUsername(string username);
		Task<T> GetByIdAsync<TId, T>(TId id);
		T GetById<TId, T>(TId id);
		Task<IEnumerable<T>> Get<T>(bool activeOnly = true);
		Task<bool> Update<T>(T model);
		IDbConnection GetConnection();
		/// <summary>
		/// Inserts a new record in an insert-only table.
		/// </summary>
		/// <typeparam name="T">table POCO</typeparam>
		/// <param name="model">table POCO</param>
		/// <returns>true</returns>
		Task<int> InsertForInsertOnly<T>(T model, int changedByUserId, IDbConnection existingConnection = null, IDbTransaction existingTransaction = null) where T : IInsertOnlyTable<int, int?>;
		Task<int> Insert<T>(T model, IDbConnection existingConnection = null, IDbTransaction existingTransaction = null);
		Task<bool> Delete<TId, T>(TId id, bool hardDelete);
		Task<bool> DeleteForInsertOnly<TId, T>(TId id, int deletedByUserId) where T : IInsertOnlyTable<int, int?>;

		Task<int> InsertNewPatientAndCoreRecord(Patient patient, IEnumerable<PatientUr> urCollection, CoreRecord CoreRecord, int changedByUserId);
		Task<int?> GetAndUpdateExistingPatientAndCoreRecord(Patient patient, IEnumerable<PatientUr> urCollection, int changedByUserId);
	}
}
