﻿using PENPals.Domain.Interfaces;
using PENPals.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PENPals.Domain
{
	public interface IReferenceData
	{
		Task<IEnumerable<T>> Get<T>() where T : IReferenceDataTable;

		Task Invalidate<T>() where T : IReferenceDataTable;

		Task Update<T>(T model) where T : IReferenceDataTable;

		Task Insert<T>(T model) where T : IReferenceDataTable;
		Task<IEnumerable<Facility>> GetFacilities();

	}
}
