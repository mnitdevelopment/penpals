﻿using MNIT.Externals.ClientDirectory;
using PENPals.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PENPals.Domain.Helpers
{
	public interface IPatientMapper
	{
		Task<Patient> MapPatient(ClientDirectoryPerson cdPerson);
		Task<IEnumerable<PatientUr>> MapUrs(ClientDirectoryPerson cdPerson);
	}

	public class ClientDirectoryPatientMapper : IPatientMapper
	{
		private readonly IReferenceData _refData;
		private const string AddressType = "R"; //Residential is preferred
		private const string PhoneType = "PM"; //Mobile phone is preferred

		public ClientDirectoryPatientMapper(IReferenceData refData)
		{
			_refData = refData;
		}
		public async Task<Patient> MapPatient(ClientDirectoryPerson cdPerson)
		{
			var genderId = (await _refData.Get<Gender>()).First(g => g.Code == cdPerson.Sex).ID;
			var indigenousStatusId = (await _refData.Get<IndigenousStatu>()).First(g => g.Code == cdPerson.IndigenousStatusID).ID;
			var patient = new Patient
			{
				PatientId = Guid.NewGuid(),
				FirstNames = cdPerson.FirstName,
				Surname = cdPerson.Surname,
				DoB = cdPerson.DOB.GetValueOrDefault(new System.DateTime(1900, 1, 1)),
				GenderId = genderId,
				Address = BuildAddress(cdPerson.Addresses),
				Phone = BuildPhone(cdPerson.Phones),
				IndigenousStatusId = indigenousStatusId,
				IsActive = true
			};
			return patient;
		}

		private string BuildAddress(List<CDAddress> addresses)
		{
			var address = addresses.FirstOrDefault(x => x.AddressType == AddressType) ?? addresses.FirstOrDefault();
			if (address == null)
			{
				return string.Empty;
			}

			return string.Format("{0} {1} {2} {3} {4}", address.Line1, address.Line2, address.Suburb, address.PostCode, address.State).Replace("  ", " ");
		}

		private string BuildPhone(List<CDPhone> phones)
		{
			var phone = phones.FirstOrDefault(x => x.PhoneType == PhoneType) ?? phones.FirstOrDefault();
			if (phone == null)
			{
				return string.Empty;
			}
			return phone.Number;
		}

		public async Task<IEnumerable<PatientUr>> MapUrs(ClientDirectoryPerson cdPerson)
		{
			var facilities = await _refData.GetFacilities();
			var urCollection = new List<PatientUr>();
			foreach (var ur in cdPerson.URNumbers)
			{
				var facility = facilities.FirstOrDefault(f => f.HICCode == ur.FacilityHicCode);
				if (facility != null)
				{
					urCollection.Add(new PatientUr
					{
						FacilityId = facility.ID,
						URN = ur.URNumber
					});
				}
			}
			return urCollection;
		}
	}
}
