﻿using System;
using Autofac;
using MNIT.ApplicationAuditing;
using MNIT.ApplicationAuditing.Local.Domain;
using MNIT.Caching;
using MNIT.ErrorLogging;
using MNIT.ErrorLogging.Local.Domain;
using PENPals.Domain.Helpers;

namespace PENPals.Domain
{
	public class IoCConfig
	{
		public static void BuildDomainContainer(ContainerBuilder builder)
		{
			builder.Register(context => new LocalAuditRepository("PENPals")).As<IAuditRepository>();
			builder.Register(context => new LocalErrorRepository("PENPals")).As<IErrorRepository>();
			builder.RegisterType<Repository>().As<IRepository>();
			builder.RegisterType<ClientDirectoryPatientMapper>().As<IPatientMapper>();

			builder.RegisterType<CacheService>().As<ICacheService>();
			//TODO: Need to resolve this
			builder.RegisterType<TempCacheServiceAuditHelper>().As<ICacheServiceAuditHelper>();
			builder.RegisterType<MemoryCacheProvider>().As<ICacheProvider>();
			builder.RegisterType<ReferenceData>().As<IReferenceData>();
		}
	}

	internal class TempCacheServiceAuditHelper : ICacheServiceAuditHelper
	{
		public void AuditAction(object entity, string auditType)
		{
			//no-op
		}

		public void AuditAction(object entity, string auditType, string auditDetails)
		{
			//no-op
		}
	}
}
