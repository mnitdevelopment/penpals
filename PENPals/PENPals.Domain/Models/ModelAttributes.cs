﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PENPals.Domain.Models
{
	/// <summary>
	/// Optional OuterKey attribute.
	/// Specifies that this property represents the 1 in a 1:N FK relationship
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class OuterKeyAttribute : Attribute
	{
	}

	/// <summary>
	/// Optional InnerKey attribute.
	/// Specifies that this property represents the N in a 1:N FK relationship
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class InnerKeyAttribute : Attribute
	{
	}
}
