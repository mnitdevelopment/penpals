﻿using Dapper;

namespace PENPals.Domain.Models
{
	public partial class Patient
	{
		[ReadOnly(true)]
		public int? CoreRecordId { get; set; }

		[ReadOnly(true)]
		public string URNs { get; set; }
	}
}
