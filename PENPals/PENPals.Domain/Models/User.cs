﻿using MNIT.Web.Security.Models;
using PENPals.Domain.Interfaces;
using System.ComponentModel.DataAnnotations;
using System;

namespace PENPals.Domain.Models
{
	public partial class User : IAdminTable, IAppUser<int>
	{
		int IAppUser<int>.ID
		{
			get
			{
				return this.ID;
			}

			set
			{
				this.ID = value;
			}
		}

		bool IAppUser<int>.IsAdmin
		{
			get
			{
				return this.IsAdmin.GetValueOrDefault(false);
			}

			set
			{
				this.IsAdmin = value;
			}
		}
	}
}
