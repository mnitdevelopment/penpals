﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PENPals.Domain.Interfaces
{
	public interface IInsertOnlyTable<T, T2>
	{
		T ID { get; set; }
		T2 BaseVersion { get; set; }
		DateTime ChangedAt { get; set; }
		bool IsActive { get; set; }
		int ChangedById { get; set; }

	}
}
