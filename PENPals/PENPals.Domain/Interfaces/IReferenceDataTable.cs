﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PENPals.Domain.Interfaces
{
	public interface IReferenceDataTable
	{
		short ID { get; set; }
		string Code { get; set; }
		string Name { get; set; }
		bool IsActive { get; set; }
	}
}
