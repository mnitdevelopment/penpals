﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace PENPals.Tests
{
	[TestClass]
	public class OctoDatabaseCheck
	{
		[TestMethod]
		public void CheckFilesAreCopyAlways()
		{
			//Find the project file - needs to be resilient to being run locally or on the TFS server
			var projectFile = "PENPals.Database.csproj";
			var projPath = string.Empty;
			var searchPath = new System.IO.DirectoryInfo(System.Environment.CurrentDirectory);
			var safety = 0;
			while (safety < 5)
			{
				var matches = System.IO.Directory.GetFiles(searchPath.FullName, projectFile, System.IO.SearchOption.AllDirectories);
				if (matches.Any())
				{
					projPath = matches[0];
					break;
				}
				searchPath = searchPath.Parent;
				safety++;
			}
			XDocument doc = XDocument.Load(projPath);
			var files = from f in doc.Descendants()
						where f.Name.LocalName == "Content" || f.Name.LocalName == "None"
						select new
						{
							BuildAction = f.Name.LocalName,
							Content = f.Attribute("Include").Value,
							Copy = f.Descendants().ToList()
						};
			Assert.IsTrue(files.Any(f => f.Content.EndsWith(".sql")), "There are no .sql files in your Database project.");
			foreach (var file in files)
			{
				if (file.Content.EndsWith(".sql"))
				{
					Assert.IsTrue(file.BuildAction == "Content", file.Content + " is not marked as Content");
					Assert.IsTrue(file.Copy.Count > 0, file.Content + " is not set to Copy Local");
				}
				if (file.Content.EndsWith("rh.exe"))
				{
					Assert.IsTrue(file.BuildAction == "Content", file.Content + " is not marked as Content");
					Assert.IsTrue(file.Copy.Count > 0, file.Content + " is not set to Copy Local");
				}
				if (file.Content.EndsWith("deploy.database.ps1"))
				{
					Assert.IsTrue(file.BuildAction == "Content", file.Content + " is not marked as Content");
					Assert.IsTrue(file.Copy.Count > 0, file.Content + " is not set to Copy Local");
				}
			}
		}

	}
}
