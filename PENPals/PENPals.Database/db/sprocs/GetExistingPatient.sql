﻿if exists(select * from sys.objects where name = 'GetExistingPatient')
	drop proc GetExistingPatient
go

create proc GetExistingPatient
(
	@facilityId int,
	@ur varchar(10)
)
as
begin

select vw.*
from PatientUr u
inner join Patient p on p.ID = u.PatientId 
inner join vwCurrentPatient vw on vw.ID = p.ID
where u.FacilityId = @facilityId and u.URN = @ur

end
go 
