﻿DECLARE @Name VarChar(100),@Type VarChar(20), @Schema VarChar(20)
			SELECT @Name = 'hp_Admin_ConfigureEnvironmentSecurity', @Type = 'P', @Schema = 'dbo'

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(@Schema + '.' +  @Name))
BEGIN
  DECLARE @SQL varchar(1000)
  SET @SQL = 'CREATE proc ' + @Schema + '.' + @Name + ' AS SELECT * FROM sys.objects'
  EXECUTE(@SQL)
END 

GO
alter proc [dbo].[hp_Admin_ConfigureEnvironmentSecurity]
as
set nocount on

-- Proc Config
-- ============================================================================
declare @AppName nvarchar(25) = 'PENPals'

-- Determine database environment and prepare suffixes
declare @EnvSuffix nvarchar(20), @EnvPasswordSuffix nvarchar(20)
if (db_name() like '%DEV%')			select @EnvSuffix = 'dev'
else if (db_name() like '%TEST%')	select @EnvSuffix = 'test'
else if (db_name() like '%UAT%')	select @EnvSuffix = 'uat'
else if (db_name() like '%PROD%')	select @EnvSuffix = 'prod'
else begin raiserror('Unable to determine environment from database name!', 16, 1); return; end
-- ============================================================================


-- Begin by dropping any existing users for the application.
-- This will get remnant users from other environments too (ie. we're configuring test, so we want dev users removed).
declare @dropUserSql nvarchar(max)
select	@dropUserSql = coalesce(@dropUserSql + '; drop user ' + name, 'drop user ' + name)
from	sys.database_principals 
where	name like @AppName + '%'
		and [type] = 'S'
print	'-- Drop all application specific users'
print	@dropUserSql
exec(@dropUserSql)


-- Create dynamic sql for roles / logins / users
-- Must by dynamic to allow user name prefix/suffix funkyness
declare @sql nvarchar(max) = '
-- Prepare App Role
if exists(select * from sys.database_principals where name = ''{0}_role_app'') drop role {0}_role_app
create role {0}_role_app authorization dbo
exec sp_addrolemember ''db_datareader'', ''{0}_role_app''
exec sp_addrolemember ''db_datawriter'', ''{0}_role_app''
grant execute to {0}_role_app

-- most of our tables are insert-only, don''t let data get updated/deleted in system-only tables

revoke select on ErrorLog to {0}_role_app
revoke update on ErrorLog to {0}_role_app
revoke delete on ErrorLog to {0}_role_app

revoke select on Audit to {0}_role_app
revoke update on Audit to {0}_role_app
revoke delete on Audit to {0}_role_app

-- The other tables need to have their rights reduced at creation time

-- Prepare App Login
if exists(select * from sys.server_principals where name = ''{0}_app_{1}'') drop login {0}_app_{1}
create login {0}_app_{1} with password = ''#{DatabasePassword}''

-- Prepare App User
if exists(select * from sys.database_principals where name = ''{0}_app_{1}'') drop user {0}_app_{1}
create user {0}_app_{1} for login {0}_app_{1} with default_schema=dbo
exec sp_addrolemember ''{0}_role_app'', ''{0}_app_{1}''

-- Prepare Reporting Role
if exists(select * from sys.database_principals where name = ''{0}_role_reports'') drop role {0}_role_reports
create role {0}_role_reports authorization dbo
exec sp_addrolemember ''db_datareader'', ''{0}_role_reports''
grant execute to {0}_role_reports


-- Prepare Reporting Login
if exists(select * from sys.server_principals where name = ''{0}_reports_{1}'') drop login {0}_reports_{1}
create login {0}_reports_{1} with password = ''#{ReportingPassword}''

-- Prepare Reporting User
if exists(select * from sys.database_principals where name = ''{0}_reports_{1}'') drop user {0}_reports_{1}
create user {0}_reports_{1} for login {0}_reports_{1} with default_schema=dbo
exec sp_addrolemember ''{0}_role_reports'', ''{0}_reports_{1}''
'

-- Update dynamic sql with environment-based suffixes
set @sql = replace(@sql, '{0}', @AppName)
set @sql = replace(@sql, '{1}', @EnvSuffix)

-- Print and execute changes
print @sql
exec(@sql)


GO


-- if the definition above has changed since the last run, re-run the proc so the perms take effect.
exec dbo.hp_Admin_ConfigureEnvironmentSecurity
GO
