﻿-- TODO Replace {tablename}
-- TODO Add columns and FKs

CREATE TABLE [dbo].[TreatingTeam](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TreatingTeamId] [uniqueidentifier] NOT NULL,
	-- TODO columns here
	[IsActive] [bit] NOT NULL,
	[BaseVersion] [int] NULL,
	[ChangedById] [int] NOT NULL,
	[ChangedAt] [datetime2](3) NOT NULL,
	[FacilityID] [smallint] NOT NULL,
	[Consultant] [varchar](100) NULL,
	[ConsultantPhone] [varchar](50) NULL,
	[Dietitian] [varchar](100) NULL,
	[DietitianPhone] [varchar](50) NULL,
	[Nurse] [varchar](100) NULL,
	[NursePhone] [varchar](50) NULL,
	[Other] [varchar](100) NULL,
	[IsPrimary] bit default(0) NOT NULL
 CONSTRAINT UC_TreatingTeam_BaseVersion UNIQUE(TreatingTeamId, BaseVersion),
 CONSTRAINT [FK_TreatingTeam_User]						FOREIGN KEY([ChangedById]) REFERENCES [User] ([Id]),
 CONSTRAINT [FK_TreatingTeam_TreatingTeam_BaseVersion]	FOREIGN KEY([BaseVersion]) REFERENCES [TreatingTeam] ([Id]),
 CONSTRAINT [FK_TreatingTeam_Facility]	FOREIGN KEY([FacilityID]) REFERENCES [Facility] ([Id]),
 CONSTRAINT [PK_TreatingTeam] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
go

revoke update on TreatingTeam to PENPals_role_app
revoke delete on TreatingTeam to PENPals_role_app
go

-- TODO Move to views folder
if exists(select * from sys.objects where name = 'vwCurrentTreatingTeam')
	drop view vwCurrentTreatingTeam
go

CREATE VIEW vwCurrentTreatingTeam
as 
with rankedItems as
(
	select *, row_number() over (partition by TreatingTeamId order by Id desc) rnk
	from TreatingTeam 
)
select	i.ID,
		i.TreatingTeamId,
		-- TODO columns here
		i.IsActive,
		i.BaseVersion,
		i.ChangedById,
		i.ChangedAt,
		i.FacilityID
from rankedItems i
where i.rnk = 1
