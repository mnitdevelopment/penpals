﻿insert into Gender
select 1,	'Male',	1
union all 
select 2,	'Female',	1
union all 
select 3,	'Indeterminate',	1
union all 
select 9,	'Unknown',	1

insert into IndigenousStatus
select 1,	'Aboriginal Origin',	1
union all 
select 2,	'Torres Strait Islander Origin',	1
union all 
select 3,	'Aboriginal And Torres Strait Islander Origin',	1
union all 
select 4,	'Non Indigenous',	1
union all 
select 9,	'Not Stated / Unknown',	1
