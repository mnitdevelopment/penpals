﻿CREATE TABLE [dbo].[CoreRecord](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CoreRecordId] [uniqueidentifier] NOT NULL,
	-- TODO columns here
	[PatientId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[BaseVersion] [int] NULL,
	[ChangedById] [int] NOT NULL,
	[ChangedAt] [datetime2](3) NOT NULL,
 CONSTRAINT UC_CoreRecord_BaseVersion UNIQUE(CoreRecordId, BaseVersion),
 CONSTRAINT [FK_CoreRecord_User]						FOREIGN KEY([ChangedById]) REFERENCES [User] ([Id]),
 CONSTRAINT [FK_CoreRecord_CoreRecord_BaseVersion]	FOREIGN KEY([BaseVersion]) REFERENCES [CoreRecord] ([Id]),
 CONSTRAINT [FK_CoreRecord_Patient]						FOREIGN KEY([PatientId]) REFERENCES [Patient] ([Id]),
 CONSTRAINT [PK_CoreRecord] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


revoke update on CoreRecord to PENPals_role_app
revoke delete on CoreRecord to PENPals_role_app

