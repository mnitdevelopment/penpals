﻿
CREATE TABLE [dbo].[PatientDevice](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PatientDeviceId] [uniqueidentifier] NOT NULL,
	
	[PatientId] [int] NOT NULL,
	[FeedingTypeId] [smallint] NOT NULL,
	[IndicationTypeId] [smallint] NULL,
	[InitialInsertionDate] [datetime2](3) NULL,
	[FacilityId] [smallint] NULL,
	[InterventionalistTypeId] [smallint] NULL,
	[InterventionalistName] [varchar](100) NULL,
	[AccessId] [smallint] NULL,
	[BrandId] [smallint] NULL,
	[RetentionId] [smallint] NULL,
	[AccessLocationId] [smallint] NULL,
	[SizeId] [smallint] NULL,
	[NumberOfLumensId] [smallint] NULL,
	[ImmediatePostReviewDate] [datetime2](3) NULL,
	[Clinician] [varchar](100) NULL,
	[Designation] [varchar](50) NULL,

	[IsActive] [bit] NOT NULL,
	[BaseVersion] [int] NULL,
	[ChangedById] [int] NOT NULL,
	[ChangedAt] [datetime2](3) NOT NULL,
 CONSTRAINT UC_PatientDevice_BaseVersion UNIQUE(PatientDeviceId, BaseVersion),
 CONSTRAINT [FK_PatientDevice_User]						FOREIGN KEY([ChangedById]) REFERENCES [User] ([Id]),

 CONSTRAINT [FK_PatientDevice_Patient]						FOREIGN KEY([PatientId]) REFERENCES [Patient] ([Id]),
 CONSTRAINT [FK_PatientDevice_FeedingType]						FOREIGN KEY([FeedingTypeId]) REFERENCES [FeedingType] ([Id]),
 CONSTRAINT [FK_PatientDevice_DeviceIndicationType]						FOREIGN KEY([IndicationTypeId]) REFERENCES [DeviceIndicationType] ([Id]),
 CONSTRAINT [FK_PatientDevice_Facility]						FOREIGN KEY([FacilityId]) REFERENCES [Facility] ([Id]),
 CONSTRAINT [FK_PatientDevice_InterventionalistType]						FOREIGN KEY([InterventionalistTypeId]) REFERENCES [InterventionalistType] ([Id]),
 CONSTRAINT [FK_PatientDevice_AccessType]						FOREIGN KEY([AccessId]) REFERENCES [AccessType] ([Id]),
 CONSTRAINT [FK_PatientDevice_DeviceBrand]						FOREIGN KEY([BrandId]) REFERENCES [DeviceBrand] ([Id]),
 CONSTRAINT [FK_PatientDevice_DeviceRetention]						FOREIGN KEY([RetentionId]) REFERENCES [DeviceRetention] ([Id]),
 CONSTRAINT [FK_PatientDevice_AccessLocation]						FOREIGN KEY([AccessLocationId]) REFERENCES [AccessLocation] ([Id]),
 CONSTRAINT [FK_PatientDevice_DeviceSize]						FOREIGN KEY([SizeId]) REFERENCES [DeviceSize] ([Id]),
 CONSTRAINT [FK_PatientDevice_NumberOfLumens]						FOREIGN KEY([NumberOfLumensId]) REFERENCES [NumberOfLumens] ([Id]),

 CONSTRAINT [FK_PatientDevice_PatientDevice_BaseVersion]	FOREIGN KEY([BaseVersion]) REFERENCES [PatientDevice] ([Id]),
 CONSTRAINT [PK_PatientDevice] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
go

revoke update on PatientDevice to PENPals_role_app
revoke delete on PatientDevice to PENPals_role_app
go

-- TODO Move to views folder
if exists(select * from sys.objects where name = 'vwCurrentPatientDevice')
	drop view vwCurrentPatientDevice
go

CREATE VIEW vwCurrentPatientDevice
as 
with rankedItems as
(
	select *, row_number() over (partition by PatientDeviceId order by Id desc) rnk
	from PatientDevice 
)
select	i.ID,
		i.PatientDeviceId,
		i.[PatientId],
		i.[FeedingTypeId],
		i.[IndicationTypeId],
		i.[InitialInsertionDate],
		i.[FacilityId],
		i.[InterventionalistTypeId],
		i.[InterventionalistName],
		i.[AccessId],
		i.[BrandId],
		i.[RetentionId],
		i.[AccessLocationId],
		i.[SizeId],
		i.[NumberOfLumensId],
		i.[ImmediatePostReviewDate],
		i.[Clinician],
		i.[Designation],
		i.IsActive,
		i.BaseVersion,
		i.ChangedById,
		i.ChangedAt
from rankedItems i
where i.rnk = 1
