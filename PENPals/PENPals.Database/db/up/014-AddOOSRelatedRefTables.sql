﻿-- TODO Replace {tablename}

--CREATE TABLE [dbo].[{tablename}](
--	[ID] [smallint] IDENTITY(1,1) NOT NULL,
--	[Code] [varchar](10) NOT NULL,
--	[Name] [varchar](200) NOT NULL,
--	[IsActive] [bit] NOT NULL,
-- CONSTRAINT UC_{tablename}_Code UNIQUE(Code, IsActive), --only one active code at a time please
-- CONSTRAINT [PK_{tablename}] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

-------------------------------------------------------------------------------------------


CREATE TABLE [dbo].[HHS](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT UC_HHS_Code UNIQUE(Code, IsActive), --only one active code at a time please
 CONSTRAINT [PK_HHS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

----------------------------------------------------------------------------------------------

CREATE TABLE [dbo].[EpisodeType](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT UC_EpisodeType_Code UNIQUE(Code, IsActive), --only one active code at a time please
 CONSTRAINT [PK_EpisodeType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-------------------------------------------------------------------------------------------

CREATE TABLE [dbo].[Designation](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT UC_Designation_Code UNIQUE(Code, IsActive), --only one active code at a time please
 CONSTRAINT [PK_Designation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-------------------------------------------------------------------------------------------

CREATE TABLE [dbo].[ReasonForPresentationGroup](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[FeedingTypeId] [smallint] NULL,
 CONSTRAINT UC_ReasonForPresentationGroup_Code UNIQUE(Code, IsActive), --only one active code at a time please
 CONSTRAINT [FK_ReasonForPresentationGroup_FeedingType]						FOREIGN KEY([FeedingTypeId]) REFERENCES [FeedingType] ([Id]),
 CONSTRAINT [PK_ReasonForPresentationGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-------------------------------------------------------------------------------------------

CREATE TABLE [dbo].[ReasonForPresentation](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[ReasonForPresentationGroupId] [smallint] NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT UC_ReasonForPresentation_Code UNIQUE(Code, IsActive), --only one active code at a time please
 CONSTRAINT UC_ReasonForPresentation_ReasonForPresentationGroupId_Code UNIQUE(ReasonForPresentationGroupId, Code), 
 CONSTRAINT [PK_ReasonForPresentation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-------------------------------------------------------------------------------------------

CREATE TABLE [dbo].[ComplicationGroup](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT UC_ComplicationGroup_Code UNIQUE(Code, IsActive), --only one active code at a time please
 CONSTRAINT [PK_ComplicationGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-------------------------------------------------------------------------------------------

CREATE TABLE [dbo].[Complication](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[ComplicationGroupId] [smallint] NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT UC_Complication_Code UNIQUE(Code, IsActive), --only one active code at a time please
 CONSTRAINT UC_Complication_ComplicationGroupId_Code UNIQUE(ComplicationGroupId, Code), 
 CONSTRAINT [PK_Complication] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-------------------------------------------------------------------------------------------
