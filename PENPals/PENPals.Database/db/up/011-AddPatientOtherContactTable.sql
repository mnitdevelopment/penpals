﻿-- TODO Add columns and FKs

CREATE TABLE [dbo].[OtherTreatingTeamContact](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OtherTreatingTeamContactId] [uniqueidentifier] NOT NULL,
	[PatientId] int NOT NULL,
	[Designation] [varchar](50) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Phone] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[BaseVersion] [int] NULL,
	[ChangedById] [int] NOT NULL,
	[ChangedAt] [datetime2](3) NOT NULL,
 CONSTRAINT UC_OtherTreatingTeamContact_BaseVersion UNIQUE(OtherTreatingTeamContactId, BaseVersion),
 CONSTRAINT [FK_OtherTreatingTeamContact_User]						FOREIGN KEY([ChangedById]) REFERENCES [User] ([Id]),
 CONSTRAINT [FK_OtherTreatingTeamContact_Patient]						FOREIGN KEY([PatientId]) REFERENCES [Patient] ([Id]),
 CONSTRAINT [FK_OtherTreatingTeamContact_OtherTreatingTeamContact_BaseVersion]	FOREIGN KEY([BaseVersion]) REFERENCES [OtherTreatingTeamContact] ([Id]),
 CONSTRAINT [PK_OtherTreatingTeamContact] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
go

revoke update on OtherTreatingTeamContact to PENPals_role_app
revoke delete on OtherTreatingTeamContact to PENPals_role_app
go

-- TODO Move to views folder
if exists(select * from sys.objects where name = 'vwCurrentOtherTreatingTeamContact')
	drop view vwCurrentOtherTreatingTeamContact
go

CREATE VIEW vwCurrentOtherTreatingTeamContact
as 
with rankedItems as
(
	select *, row_number() over (partition by OtherTreatingTeamContactId order by Id desc) rnk
	from OtherTreatingTeamContact 
)
select	i.ID,
		i.OtherTreatingTeamContactId,
		-- TODO columns here
		i.IsActive,
		i.BaseVersion,
		i.ChangedById,
		i.ChangedAt
from rankedItems i
where i.rnk = 1
