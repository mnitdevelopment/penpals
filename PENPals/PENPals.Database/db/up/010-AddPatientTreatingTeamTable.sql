﻿-- TODO Replace PatientTreatingTeam
-- TODO Add columns and FKs

CREATE TABLE [dbo].[PatientTreatingTeam](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PatientTreatingTeamId] [uniqueidentifier] NOT NULL,
	[PatientId] [int] NOT NULL,
	[TreatingTeamId] [int] NOT NULL,	
	[IsActive] [bit] NOT NULL,
	[BaseVersion] [int] NULL,
	[ChangedById] [int] NOT NULL,
	[ChangedAt] [datetime2](3) NOT NULL,
 CONSTRAINT UC_PatientTreatingTeam_BaseVersion UNIQUE(PatientTreatingTeamId, BaseVersion),
 CONSTRAINT [FK_PatientTreatingTeam_User]						FOREIGN KEY([ChangedById]) REFERENCES [User] ([Id]),
 CONSTRAINT [FK_PatientTreatingTeam_Patient]						FOREIGN KEY([PatientId]) REFERENCES [Patient] ([Id]),
 CONSTRAINT [FK_PatientTreatingTeam_TreatingTeam]						FOREIGN KEY([TreatingTeamId]) REFERENCES [TreatingTeam] ([Id]),
 CONSTRAINT [FK_PatientTreatingTeam_PatientTreatingTeam_BaseVersion]	FOREIGN KEY([BaseVersion]) REFERENCES [PatientTreatingTeam] ([Id]),
 CONSTRAINT [PK_PatientTreatingTeam] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
go

revoke update on PatientTreatingTeam to PENPals_role_app
revoke delete on PatientTreatingTeam to PENPals_role_app
go

-- TODO Move to views folder
if exists(select * from sys.objects where name = 'vwCurrentPatientTreatingTeam')
	drop view vwCurrentPatientTreatingTeam
go

CREATE VIEW vwCurrentPatientTreatingTeam
as 
with rankedItems as
(
	select *, row_number() over (partition by PatientTreatingTeamId order by Id desc) rnk
	from PatientTreatingTeam 
)
select	i.ID,
		i.PatientTreatingTeamId,
		i.PatientId,
		i.TreatingTeamId,
		i.IsActive,
		i.BaseVersion,
		i.ChangedById,
		i.ChangedAt
from rankedItems i
where i.rnk = 1
