﻿declare @AppName nvarchar(25) = 'PENPals'

-- Determine database environment and prepare suffixes
declare @EnvSuffix nvarchar(20), @EnvPasswordSuffix nvarchar(20)
if (db_name() like '%DEV%')			select @EnvSuffix = 'dev'
else if (db_name() like '%TEST%')	select @EnvSuffix = 'test'
else if (db_name() like '%UAT%')	select @EnvSuffix = 'uat'
else if (db_name() like '%PROD%')	select @EnvSuffix = 'prod'
else begin raiserror('Unable to determine environment from database name!', 16, 1); return; end
-- ============================================================================

-- Create dynamic sql for roles / logins / users
-- Must by dynamic to allow user name prefix/suffix funkyness
declare @sql nvarchar(max) = '
-- Prepare App Role
if exists(select * from sys.database_principals where name = ''{0}_role_app'') drop role {0}_role_app
create role {0}_role_app authorization dbo
exec sp_addrolemember ''db_datareader'', ''{0}_role_app''
exec sp_addrolemember ''db_datawriter'', ''{0}_role_app''
grant execute to {0}_role_app

-- Prepare App Login
if exists(select * from sys.server_principals where name = ''{0}_app_{1}'') drop login {0}_app_{1}
create login {0}_app_{1} with password = ''#{DatabasePassword}''

-- Prepare App User
if exists(select * from sys.database_principals where name = ''{0}_app_{1}'') drop user {0}_app_{1}
create user {0}_app_{1} for login {0}_app_{1} with default_schema=dbo
exec sp_addrolemember ''{0}_role_app'', ''{0}_app_{1}''

-- Prepare Reporting Role
if exists(select * from sys.database_principals where name = ''{0}_role_reports'') drop role {0}_role_reports
create role {0}_role_reports authorization dbo
exec sp_addrolemember ''db_datareader'', ''{0}_role_reports''
grant execute to {0}_role_reports

-- Prepare Reporting Login
if exists(select * from sys.server_principals where name = ''{0}_reports_{1}'') drop login {0}_reports_{1}
create login {0}_reports_{1} with password = ''#{ReportingPassword}''

-- Prepare Reporting User
if exists(select * from sys.database_principals where name = ''{0}_reports_{1}'') drop user {0}_reports_{1}
create user {0}_reports_{1} for login {0}_reports_{1} with default_schema=dbo
exec sp_addrolemember ''{0}_role_reports'', ''{0}_reports_{1}''
'

-- Update dynamic sql with environment-based suffixes
set @sql = replace(@sql, '{0}', @AppName)
set @sql = replace(@sql, '{1}', @EnvSuffix)

exec(@sql)

