﻿CREATE TABLE [dbo].[Facility](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[HICCode] [varchar](10) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[URPrefix] [varchar](10) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT UC_Facility_HICCode UNIQUE(HICCode, IsActive), --only one active code at a time please
 CONSTRAINT [PK_Facility] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


CREATE TABLE [dbo].[Gender](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT UC_Gender_Code UNIQUE(Code, IsActive), --only one active code at a time please
 CONSTRAINT [PK_Gender] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[IndigenousStatus](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT UC_IndigenousStatus_Code UNIQUE(Code, IsActive), --only one active code at a time please
 CONSTRAINT [PK_IndigenousStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


CREATE TABLE [dbo].[Patient](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [uniqueidentifier] NOT NULL,
	-- TODO columns here
	[FirstNames] [varchar](200) NOT NULL,
	[Surname] [varchar](200) NOT NULL,
	[DoB] [date] NOT NULL,
	[GenderId] [smallint] NOT NULL,
	[Phone] [varchar](50) NULL,
	[Address] [varchar](500) NULL,
	[IndigenousStatusId] [smallint] NULL,
	[IsActive] [bit] NOT NULL,
	[BaseVersion] [int] NULL,
	[ChangedById] [int] NOT NULL,
	[ChangedAt] [datetime2](3) NOT NULL,
	[Email] [varchar](50) NULL,
	[NextOfKinName] [varchar](100) NULL,
	[NextOfKinPhone] [varchar](50) NULL,
	[GPName] [varchar](100) NULL,
	[GPPhone] [varchar](50) NULL,
 CONSTRAINT UC_Patient_BaseVersion UNIQUE(PatientId, BaseVersion),
 CONSTRAINT [FK_Patient_User]						FOREIGN KEY([ChangedById]) REFERENCES [User] ([Id]),
 CONSTRAINT [FK_Patient_Patient_BaseVersion]	FOREIGN KEY([BaseVersion]) REFERENCES [Patient] ([Id]),
 CONSTRAINT [FK_Patient_Gender]						FOREIGN KEY([GenderId]) REFERENCES [Gender] ([Id]),
 CONSTRAINT [FK_Patient_IndigenousStatus]						FOREIGN KEY([IndigenousStatusId]) REFERENCES [IndigenousStatus] ([Id]),
 CONSTRAINT [PK_Patient] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



CREATE TABLE [dbo].[PatientUr](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[FacilityId] [smallint] NOT NULL,
	[URN] varchar(10) NOT NULL,
 CONSTRAINT [FK_PatientUr_Patient]						FOREIGN KEY([PatientId]) REFERENCES [Patient] ([Id]),
 CONSTRAINT [FK_PatientUr_Facility]	FOREIGN KEY([FacilityId]) REFERENCES [Facility] ([Id]),
 CONSTRAINT [PK_PatientUr] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


