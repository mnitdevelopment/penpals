﻿-- TODO Replace {tablename}

--CREATE TABLE [dbo].[{tablename}](
--	[ID] [smallint] IDENTITY(1,1) NOT NULL,
--	[Code] [varchar](10) NOT NULL,
--	[Name] [varchar](200) NOT NULL,
--	[IsActive] [bit] NOT NULL,
-- CONSTRAINT UC_{tablename}_Code UNIQUE(Code, IsActive), --only one active code at a time please
-- CONSTRAINT [PK_{tablename}] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]



CREATE TABLE [dbo].[FeedingType](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT UC_FeedingType_Code UNIQUE(Code, IsActive), --only one active code at a time please
 CONSTRAINT [PK_FeedingType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

insert into FeedingType
values	('PEG', 'Gastrostomy', 1),
		('PN', 'PN', 1)
GO

-----------------------------------------------------------------------

CREATE TABLE [dbo].[DeviceIndicationType](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[FeedingTypeId] [smallint] NULL,
 CONSTRAINT UC_DeviceIndicationType_Code UNIQUE(Code, IsActive), --only one active code at a time please
 CONSTRAINT [FK_DeviceIndicationType_FeedingType]						FOREIGN KEY([FeedingTypeId]) REFERENCES [FeedingType] ([Id]),
 CONSTRAINT [PK_DeviceIndicationType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
-----------------------------------------------------------------------------

CREATE TABLE [dbo].[InterventionalistType](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[FeedingTypeId] [smallint] NULL,
 CONSTRAINT UC_InterventionalistType_Code UNIQUE(Code, IsActive), --only one active code at a time please
 CONSTRAINT [FK_InterventionalistType_FeedingType]						FOREIGN KEY([FeedingTypeId]) REFERENCES [FeedingType] ([Id]),
 CONSTRAINT [PK_InterventionalistType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

---------------------------------------------------------------------------------

CREATE TABLE [dbo].[AccessType](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[FeedingTypeId] [smallint] NULL,
 CONSTRAINT UC_AccessType_Code UNIQUE(Code, IsActive), --only one active code at a time please
 CONSTRAINT [FK_AccessType_FeedingType]						FOREIGN KEY([FeedingTypeId]) REFERENCES [FeedingType] ([Id]),
 CONSTRAINT [PK_AccessType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
------------------------------------------------------------------------------------

CREATE TABLE [dbo].[DeviceBrand](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[FeedingTypeId] [smallint] NULL,
 CONSTRAINT UC_DeviceBrand_Code UNIQUE(Code, IsActive), --only one active code at a time please
 CONSTRAINT [FK_DeviceBrand_FeedingType]						FOREIGN KEY([FeedingTypeId]) REFERENCES [FeedingType] ([Id]),
 CONSTRAINT [PK_DeviceBrand] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------

CREATE TABLE [dbo].[AccessLocation](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT UC_AccessLocation_Code UNIQUE(Code, IsActive), --only one active code at a time please
 CONSTRAINT [PK_AccessLocation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

------------------------------------------------------------------------------------------

CREATE TABLE [dbo].[DeviceSize](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[FeedingTypeId] [smallint] NULL,
 CONSTRAINT UC_DeviceSize_Code UNIQUE(Code, IsActive), --only one active code at a time please
 CONSTRAINT [FK_DeviceSize_FeedingType]						FOREIGN KEY([FeedingTypeId]) REFERENCES [FeedingType] ([Id]),
 CONSTRAINT [PK_DeviceSize] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

---------------------------------------------------------------------------------------------

CREATE TABLE [dbo].[DeviceRetention](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[FeedingTypeId] [smallint] NULL,
 CONSTRAINT UC_DeviceRetention_Code UNIQUE(Code, IsActive), --only one active code at a time please
 CONSTRAINT [FK_DeviceRetention_FeedingType]						FOREIGN KEY([FeedingTypeId]) REFERENCES [FeedingType] ([Id]),
 CONSTRAINT [PK_DeviceRetention] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
-----------------------------------------------------------------------------------------

CREATE TABLE [dbo].[NumberOfLumens](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT UC_NumberOfLumens_Code UNIQUE(Code, IsActive), --only one active code at a time please
 CONSTRAINT [PK_NumberOfLumens] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
