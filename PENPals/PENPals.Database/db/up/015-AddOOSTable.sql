﻿-- TODO Replace PatientOccasionOfService
-- TODO Add columns and FKs

CREATE TABLE [dbo].[PatientOccasionOfService](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PatientOccasionOfServiceId] [uniqueidentifier] NOT NULL,
	
	PatientId int NOT NULL,
	FeedingTypeId smallint NOT NULL,
	OOSDate datetime2(3) NULL,
	FacilityID smallint NULL,
	EpisodeTypeId smallint NULL,
	Clinician varchar(100) NULL,
	ClinicianDesignationId smallint NULL,
	Comments varchar(1000) NULL,
	CommentsRequired bit default(0) NOT NULL,
	ReasonForPresentationGroupId smallint NULL,
	ReasonForPresentationId smallint NULL,
	PrimaryComplicationGroupId smallint NULL,
	PrimaryComplicationId smallint NULL,
	PrimaryComplicationOther varchar(100) NULL,
	SecondaryComplicationGroupId smallint NULL,
	SecondaryComplicationId smallint NULL,
	SecondaryComplicationOther varchar(100) NULL,

	[IsActive] [bit] NOT NULL,
	[BaseVersion] [int] NULL,
	[ChangedById] [int] NOT NULL,
	[ChangedAt] [datetime2](3) NOT NULL,
 CONSTRAINT UC_PatientOccasionOfService_BaseVersion UNIQUE(PatientOccasionOfServiceId, BaseVersion),
 CONSTRAINT [FK_PatientOccasionOfService_User]						FOREIGN KEY([ChangedById]) REFERENCES [User] ([Id]),

 CONSTRAINT [FK_PatientOccasionOfService_Patient]						FOREIGN KEY([PatientId]) REFERENCES [Patient] ([Id]),
 CONSTRAINT [FK_PatientOccasionOfService_FeedingType]						FOREIGN KEY([FeedingTypeId]) REFERENCES [FeedingType] ([Id]),
 CONSTRAINT [FK_PatientOccasionOfService_Facility]						FOREIGN KEY([FacilityId]) REFERENCES [Facility] ([Id]),
 CONSTRAINT [FK_PatientOccasionOfService_EpisodeType]						FOREIGN KEY([EpisodeTypeId]) REFERENCES [EpisodeType] ([Id]),
 CONSTRAINT [FK_PatientOccasionOfService_Designation]						FOREIGN KEY([ClinicianDesignationId]) REFERENCES [Designation] ([Id]),
 CONSTRAINT [FK_PatientOccasionOfService_ReasonForPresentationGroup]						FOREIGN KEY([ReasonForPresentationGroupId]) REFERENCES [ReasonForPresentationGroup] ([Id]),
 CONSTRAINT [FK_PatientOccasionOfService_ReasonForPresentation]						FOREIGN KEY([ReasonForPresentationId]) REFERENCES [ReasonForPresentation] ([Id]),
 CONSTRAINT [FK_PatientOccasionOfService_PrimaryComplicationGroup]						FOREIGN KEY([PrimaryComplicationGroupId]) REFERENCES [ComplicationGroup] ([Id]),
 CONSTRAINT [FK_PatientOccasionOfService_PrimaryComplication]						FOREIGN KEY([PrimaryComplicationId]) REFERENCES [Complication] ([Id]),
 CONSTRAINT [FK_PatientOccasionOfService_SecondaryComplicationGroup]						FOREIGN KEY([SecondaryComplicationGroupId]) REFERENCES [ComplicationGroup] ([Id]),
 CONSTRAINT [FK_PatientOccasionOfService_SecondaryComplication]						FOREIGN KEY([SecondaryComplicationId]) REFERENCES [Complication] ([Id]),

 CONSTRAINT [FK_PatientOccasionOfService_PatientOccasionOfService_BaseVersion]	FOREIGN KEY([BaseVersion]) REFERENCES [PatientOccasionOfService] ([Id]),
 CONSTRAINT [PK_PatientOccasionOfService] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
go

revoke update on PatientOccasionOfService to PENPals_role_app
revoke delete on PatientOccasionOfService to PENPals_role_app
go

-- TODO Move to views folder
if exists(select * from sys.objects where name = 'vwCurrentPatientOccasionOfService')
	drop view vwCurrentPatientOccasionOfService
go

CREATE VIEW vwCurrentPatientOccasionOfService
as 
with rankedItems as
(
	select *, row_number() over (partition by PatientOccasionOfServiceId order by Id desc) rnk
	from PatientOccasionOfService 
)
select	i.ID,
		i.PatientOccasionOfServiceId,
		
		i.PatientId,
		i.FeedingTypeId,
		i.OOSDate ,
		i.FacilityID ,
		i.EpisodeTypeId ,
		i.Clinician ,
		i.ClinicianDesignationId ,
		i.Comments ,
		i.CommentsRequired ,
		i.ReasonForPresentationGroupId ,
		i.ReasonForPresentationId ,
		i.PrimaryComplicationGroupId ,
		i.PrimaryComplicationId ,
		i.PrimaryComplicationOther ,
		i.SecondaryComplicationGroupId ,
		i.SecondaryComplicationId ,
		i.SecondaryComplicationOther ,

		i.IsActive,
		i.BaseVersion,
		i.ChangedById,
		i.ChangedAt
from rankedItems i
where i.rnk = 1
