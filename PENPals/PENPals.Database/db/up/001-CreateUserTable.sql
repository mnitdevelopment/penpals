﻿
CREATE TABLE [dbo].[User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[ActualName] [varchar](50) NULL,
	[EmailAddress] [varchar](200) NULL,
	[PhoneNumber] [varchar](50) NULL,
	[IsAdmin] [bit] NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UX_User] UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT INTO [User]
SELECT 'frasejon', 'Jon Fraser', 'Jon.Fraser@health.qld.gov.au', '', 1, 1
UNION ALL
SELECT 'chaoura', 'Alex Chaourov', 'Alex.Chaourov@health.qld.gov.au', '', 1, 1
UNION ALL
SELECT 'boscoes', 'Shane Boscoe', 'Shane.Boscoe@health.qld.gov.au', '', 1, 1
UNION ALL
SELECT 'lazom', 'Mary Lazo', 'Mary.Lazo@health.qld.gov.au', '', 1, 1
UNION ALL
SELECT 'thompsti', 'Tim Thompson', 'Tim.Thompson@health.qld.gov.au', '', 1, 1
UNION ALL
SELECT 'missendeni', 'Ian Missenden', 'Ian.Missenden@health.qld.gov.au', '', 1, 1
UNION ALL
SELECT 'kanowskia', 'Anthony Kanowski', 'Anthony.Kanowski@health.qld.gov.au', '', 1, 1
UNION ALL
SELECT 'wiltshih', 'Hayden Wiltshire', 'Hayden.Wiltshire@health.qld.gov.au', '', 1, 1
UNION ALL
SELECT 'perskest', 'Steven Perske', 'Steven.Perske@health.qld.gov.au', '', 1, 1
UNION ALL
SELECT 'harvelau', 'Laurence Harvey', 'Laurence.Harvey@health.qld.gov.au', '', 1, 1
UNION ALL
SELECT 'tchitaevm', 'Maxim Tchitaev', 'Maxim.Tchitaev@health.qld.gov.au', '', 1, 1
