﻿-- Remove the security sproc in production as we don't want a prod restore to open up credentials

-- This will only run once as there should never be a change detected, but that's ok, we should only be 
-- configuring production security once :)

drop proc hp_Admin_ConfigureEnvironmentSecurity
