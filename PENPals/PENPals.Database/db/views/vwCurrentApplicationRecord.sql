﻿CREATE VIEW vwCurrentCoreRecord
as 
with rankedItems as
(
	select *, row_number() over (partition by CoreRecordId order by Id desc) rnk
	from CoreRecord 
)
select	i.ID,
		i.CoreRecordId,
		-- TODO columns here
		i.PatientId,
		i.IsActive,
		i.BaseVersion,
		i.ChangedById,
		i.ChangedAt
from rankedItems i
where i.rnk = 1
