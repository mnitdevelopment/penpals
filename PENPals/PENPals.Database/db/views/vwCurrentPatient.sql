﻿if exists(select * from sys.objects where name = 'vwCurrentPatient')
	drop view vwCurrentPatient
go

CREATE VIEW vwCurrentPatient
as 
with rankedItems as
(
	select p.*, 
	
	substring(
        (
            Select ', ' + f.URPrefix + ur.URN AS [text()]
            From PatientUr ur
			inner join Facility f on ur.FacilityId = f.ID
            Where ur.PatientId = p.ID
            ORDER BY f.URPrefix
            For XML PATH ('')
        ), 2, 100) URNs,
			
	r.ID as CoreRecordId, row_number() over (partition by p.PatientId order by p.Id desc) rnk
	from Patient p
	inner join CoreRecord r on p.ID = r.PatientId
)
select	i.ID,
		i.PatientId,
		[FirstNames] ,
	[Surname] ,
	[DoB] ,
	[GenderId] ,
	[Phone] ,
	[Address],
	[IndigenousStatusId] ,
	i.URNs,
	i.CoreRecordId,
	-- TODO columns here
		i.IsActive,
		i.BaseVersion,
		i.ChangedById,
		i.ChangedAt
from rankedItems i
where i.rnk = 1
