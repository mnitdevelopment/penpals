﻿@echo off

SET sqlfiles.directory="..\db\"
SET server.database="localhost\sql2014"
SET environment=DEV
SET database.name="PENPals_%environment%"
SET rh.path=".\"
SET version.file="..\bin\Debug\PENPals.Database.dll"

"%rh.path%\rh.exe" /silent /d=%database.name% /s=%server.database% /drop
"%rh.path%\rh.exe" /silent /d=%database.name% /s=%server.database% /f=..\db\ /env=%environment% /vf=%version.file% /simple 

pause
