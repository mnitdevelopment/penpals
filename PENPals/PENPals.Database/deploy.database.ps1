﻿# Run the deployment file
$server_database="#{DatabaseServer}"
$environment="#{ApplicationEnvironment}"
$database_name="#{ApplicationNamePrefix}_$environment"
$rh_path=".\deployment"
$version_file=".\#{ApplicationNamePrefix}.Database.dll"

Write-Host("Upgrading $database_name on $server_database...")

cmd.exe "/c $rh_path\rh.exe /silent /d=$database_name /s=$server_database /f=.\db\ /env=$environment /vf=$version_file /simple"
